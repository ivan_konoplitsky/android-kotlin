package com.example.android.marsrealestate.bearing.network

import com.squareup.moshi.JsonClass


@JsonClass(generateAdapter = true)
data class NetworkBearingContant(val content: List<NetworkBearing>)


@JsonClass(generateAdapter = true)
data class NetworkBearing(
        val model: String,
        val manufacturer: String,
        val url: String,
        val country: String,
        val size: String,
        val exist: String?)


//fun NetworkBearingContant.asDatabaseModel(): List<DatabaseVideo> {
//    return content.map {
//        DatabaseVideo(
//                title = it.model,
//                description = it.manufacturer,
//                url = it.url,
//                updated = it.size,
//                thumbnail = it.country)
//    }
//}

