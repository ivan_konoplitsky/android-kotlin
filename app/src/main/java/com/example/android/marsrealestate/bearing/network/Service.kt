package com.example.android.marsrealestate.bearing.network

import com.jakewharton.retrofit2.adapter.kotlin.coroutines.CoroutineCallAdapterFactory
import kotlinx.coroutines.Deferred
import retrofit2.Retrofit
import retrofit2.converter.moshi.MoshiConverterFactory
import retrofit2.http.GET


interface DevbyteService {
//    @GET("devbytes")
    @GET("bearing")
    fun getPlaylist(): Deferred<NetworkBearingContant>
}


object DevByteNetwork {

    // Configure retrofit to parse JSON and use coroutines
    private val retrofit = Retrofit.Builder()
//            .baseUrl("https://android-kotlin-fun-mars-server.appspot.com/")
            .baseUrl("http://q11.jvmhost.net/")
            .addConverterFactory(MoshiConverterFactory.create())
            .addCallAdapterFactory(CoroutineCallAdapterFactory())
            .build()

    val devbytes = retrofit.create(DevbyteService::class.java)

}


