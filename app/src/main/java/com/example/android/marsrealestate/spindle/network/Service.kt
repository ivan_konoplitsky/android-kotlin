package com.example.android.marsrealestate.spindle.network

import com.jakewharton.retrofit2.adapter.kotlin.coroutines.CoroutineCallAdapterFactory
import kotlinx.coroutines.Deferred
import retrofit2.Retrofit
import retrofit2.converter.moshi.MoshiConverterFactory
import retrofit2.http.GET


interface SpindleService {
    @GET("spindle")
    fun getspindleList(): Deferred<NetworkSpindleContant>
}


object SpindleNetwork {

    private val retrofit = Retrofit.Builder()
            .baseUrl("http://q11.jvmhost.net/")
            .addConverterFactory(MoshiConverterFactory.create())
            .addCallAdapterFactory(CoroutineCallAdapterFactory())
            .build()

    val spindleService: SpindleService = retrofit.create(SpindleService::class.java)

}


